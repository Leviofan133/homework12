﻿
#include <iostream>
#include <string>

int main()
{
    std::string AddWord;
    std::cout << "Write word or string: ";
    getline(std::cin, AddWord);
    std::cout << "\n" << "Your word or string is: " << AddWord << "\n";

    // Первый метод подсчёта символов, на насколько я понял, он выдаёт size_t, а не int
    // Что не даёт впоследствии захватить это значение, чтобы выбрать последний символ
    std::cout << "\n" << "Count of symbols (variant 1) = " << AddWord.length() << "\n";

    // Метод подсчёта символов перебором
    unsigned count{};
    for (char a : AddWord)
    {
        if (a != '\n')
        {
            count++;
        }
    }
    std::cout << "\n" << "Count of symbols (variant 2) = " << count << "\n";

    // Первый символ строки
    char b { AddWord [0] };
    std::cout << "\n" << "First symbol - " << b << "\n";

    // Последний символ строки
    // char c { AddWord [AddWord.length()] }; // - не работает, как перевести unsigned __int64 (size_t) к int я не нашёл

    char c { AddWord [count-1] }; // минус один обусловлен тем, что начинаем массив с нуля
    std::cout << "\n" << "Last symbol - " << c << "\n";

    }